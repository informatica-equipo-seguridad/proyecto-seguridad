import json
import tweepy

#Se debe crear el file keys.json tomando como refencia el file ejemplo_keys.json

with open('keys.json', 'r') as f:
    credentials = json.load(f)

consumer_key = credentials['consumer_key']
consumer_secret = credentials['consumer_secret']
access_token = credentials['access_token']
access_token_secret = credentials['access_token_secret']

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

for tweet in tweepy.Cursor(api.search,q="ladron",count=10,geocode="4.67141,-74.14598,20km",lang="es").items(10):
    print([tweet.created_at, tweet.text.encode('utf-8'), tweet.user.id, tweet.geo])
